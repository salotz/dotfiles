(cons*
 (channel
  (name 'non-free-applications)
  (url "https://gitlab.com/guix-gaming-channels/applications.git")
  (branch "master"))
 (channel
  (name 'non-free-games)
  (url "https://gitlab.com/guix-gaming-channels/games.git")
  (branch "master"))
 (channel
  (name 'non-free-duke-nukem-3d)
  (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
  (branch "master"))
 ;; (channel
 ;;  (name 'guix-chromium)
 ;;  (url "https://gitlab.com/mbakke/guix-chromium.git")
 ;;  (branch "master"))
 %default-channels)
