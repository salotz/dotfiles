;; Run as ‘guix pull -C FILE’.

(cons*
 (channel
  (name 'non-free-applications)
  (url "https://gitlab.com/guix-gaming-channels/applications.git")
  (branch "master"))
 (channel
  (name 'non-free-games)
  (url "https://gitlab.com/guix-gaming-channels/games.git")
  (branch "master"))
 (channel
  (name 'non-free-duke-nukem-3d)
  (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
  (branch "master"))
 ;; The following channel can be used as an alternative to
 ;; %default-channels to avoid rebuilding Guix on =guix pull=.
 (list
  (channel
   (inherit (first %default-channels))
   ;; Get commit from =guix describe -f channels=.
   (commit "5069baedb8a902c3b1ea9656c11471658a1de56b"))))
