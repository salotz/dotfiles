;; With Eshell:
;; guix system disk-image ~/.guix-packages/ambrevar/system/usb-install-media.scm
;; setq GUIX_DISKIMAGE ${guix system disk-image ~/.guix-packages/ambrevar/system/usb-install-media.scm} && cp $GUIX_DISKIMAGE ~/temp && guix gc --delete $GUIX_DISKIMAGE

(define-module (ambrevar system usb-install-media)
  #:use-module (ambrevar system default)
  #:use-module (gnu))

(use-package-modules bootloaders)

(operating-system
  (inherit default-operating-system)
  ;; For some reason USB media can only be booted with GRUB set to "bios" mode.
  ;; It could have been possible to specialize default-operating-system
  ;; depending on (command-line) containing "reconfigure" or "disk-image", but
  ;; that's brittle and less explicit to the user.
  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (target "/dev/sda")))
  (file-systems (cons* (file-system
                         (device (file-system-label "guixsd"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device "none")
                         (mount-point "/tmp")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems)))
