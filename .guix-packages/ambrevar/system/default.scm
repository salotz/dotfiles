(define-module (ambrevar system default)
  #:use-module (gnu)
  #:use-module (gnu system nss)
  #:use-module (nonfree packages linux)
  #:use-module (gnu packages linux)  ; For ntfs-3g
  #:use-module (gnu packages mtools)    ; For exfat
  #:use-module (gnu packages emacs-xyz)    ; For EXWM
  #:use-module (gnu packages lisp)    ; For stumpwm
  #:use-module (gnu services xorg)
  #:use-module (gnu services networking) ; For tor
  #:use-module (gnu packages xorg))

(use-service-modules desktop)
(use-package-modules certs)
(use-package-modules gnome)
;; (use-package-modules bootloaders certs suckless) ; xorg

;; Allow members of the "video" group to change the screen brightness.
(define %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

;; Mount Nitrokey
;; TODO: Check if plugdev works instead of users.  If not, report to Nitrokey.
;; https://www.nitrokey.com/sites/default/files/41-nitrokey.rules
;; (define %nitrokey-udev-rule
;;   (udev-rule
;;    "41-nitrokey.rules"
;;    (string-append "ACTION==\"add\", SUBSYSTEM==\"usb\", "
;;                   "ATTR{idVendor}==\"20a0\", ATTR{idProduct}==\"4211\", "
;;                   "ENV{ID_SMARTCARD_READER}=\"1\", ENV{ID_SMARTCARD_READER_DRIVER}=\"gnupg\", GROUP+=\"users\", MODE=\"0666\"")))
;; (define %nitrokey-udev-rule
;;   (file->udev-rule
;;    "41-nitrokey.rules"
;;    (let ((version "20170910"))
;;      (origin
;;        (method url-fetch)
;;        (uri "https://www.nitrokey.com/sites/default/files/41-nitrokey.rules")
;;        (sha256
;;         (base32 "127nghkfd4dl5mkf5xl1mij2ylxhkgg08nlh912xwrrjyjv4y9sa"))))))

(define %mimimi/xorg-modules
  ;; Everything but Nouveau.
  (delete xf86-video-nouveau %default-xorg-modules))

;; TODO: Move this to laptop-mimimi.
(define %mimimi/xorg-touchpad
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ClickMethod\" \"clickfinger\"
  Option \"ScrollMethod\" \"twofinger\"
  Option \"NaturalScrolling\" \"true\"
EndSection")

;; Use the "desktop" services, which include the X11
;; log-in service, networking with Wicd, and more.
(define %mimimi/services
  (modify-services
      %desktop-services
    ;; (guix-service-type config =>
    ;;                    (guix-configuration
    ;;                     (inherit config)
    ;;                     ;; Don't clean build deps.
    ;;                     ;; See (info "(guix) Invoking guix-daemon").
    ;;                     ;; WARNING: This tends to yield an ever-growing store.
    ;;                     (extra-options '("--gc-keep-outputs"))
    ;;                     ;; Specify substitutes manually.  Defaults
    ;;                     ;; should be good enough in most cases.
    ;;                     (substitute-urls
    ;;                      (list
    ;;                       "https://berlin.guixsd.org"
    ;;                       "https://mirror.hydra.gnu.org"
    ;;                       "https://hydra.gnu.org"))))
    (udev-service-type config =>
                       (udev-configuration
                        (inherit config)
                        (rules (append (udev-configuration-rules config)
                                       (list ;; %nitrokey-udev-rule
                                        %backlight-udev-rule)))))
    (slim-service-type config =>
                       (slim-configuration
                        (inherit config)
                        (auto-login? #f)
                        (xorg-configuration
                         (xorg-configuration
                          (modules %mimimi/xorg-modules)
                          (extra-config (list %mimimi/xorg-touchpad))))
                        ;; TODO: Can't slim pre-fill the username?
                        (default-user "ambrevar")))))

(define %mimimi/tor-config
  "ExitNodes {se},{nl},{fr},{ee},{no},{dk},{fi}
StrictNodes 1")

(define-public default-operating-system
  (operating-system
    (host-name "mimimi")
    (timezone "Europe/Paris")
    (locale "en_US.utf8")

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader (bootloader-configuration
                 (bootloader grub-efi-bootloader)
                 (timeout 1)
                 (target "/boot/efi")))

    (kernel linux-nonfree)
    ;; (kernel-arguments '("modprobe.blacklist=pcspkr"))
    ;; (kernel-arguments '("modprobe.blacklist=nouveau"))
    ;; (kernel-arguments '("pcie_port_pm=off"))
    (firmware (append (list linux-firmware-iwlwifi)
                      %base-firmware))
    ;; TODO: Remove all virtio modules.
    (initrd-modules (delete "virtio-rng" %base-initrd-modules))

    ;; Assume the target root file system is labelled "guixsd",
    ;; and the EFI System Partition is specified by its UUID.
    (file-systems (cons* (file-system
                           (device (file-system-label "guixsd"))
                           (mount-point "/")
                           (type "ext4"))
                         ;; (file-system
                         ;;   (device (file-system-label "home"))
                         ;;   (mount-point "/home")
                         ;;   (type "ext4"))
                         (file-system
                           (device (uuid "4E30-891F" 'fat))
                           (mount-point "/boot/efi")
                           (type "vfat"))
                         (file-system
                           (mount-point "/tmp")
                           (device "none")
                           (type "tmpfs")
                           (check? #f))
                         %base-file-systems))

    ;; (groups (cons* (user-group
    ;;                 (name "plugdev")
    ;;                 (system? #t))
    ;;                %base-groups))
    (users (cons* (user-account
                   (name "ambrevar")
                   (group "users")
                   (supplementary-groups '("wheel" "netdev" ; netdev is needed for networking.
                                           ;; "plugdev"        ; nitrokey?
                                           ;; "audio"
                                           "lp" ; for bluetooth
                                           "video"))
                   ;; TODO: Default to name?
                   (home-directory "/home/ambrevar"))
                  %base-user-accounts))

    (packages (cons* nss-certs          ;for HTTPS access
                     ;; gvfs               ;for user mounts
                     ntfs-3g
                     exfat-utils
                     fuse-exfat
                     stumpwm
                     emacs-exwm
                     %base-packages))

    (services (cons*
               ;; TODO: The following service starts too soon and results in a
               ;; kernel panic because /sys/... is not found.
               ;; (simple-service 'my-/sys-tweaks activation-service-type
               ;;                 ;; >> echo '1' > '/sys/module/snd_hda_intel/parameters/power_save';
               ;;                 #~(call-with-output-file "/sys/module/snd_hda_intel/parameters/power_save"
               ;;                     (lambda (port)
               ;;                       (display "1" port)))
               ;;                 ;; >> echo 'auto' > '/sys/bus/usb/devices/1-6/power/control';
               ;;                 ;; >> echo 'auto' > '/sys/bus/usb/devices/1-7/power/control';
               ;;                 ;; >> echo 'auto' > '/sys/bus/i2c/devices/i2c-2/device/power/control';
               ;;                 ;; >> echo 'auto' > '/sys/bus/pci/devices/0000:02:00.0/power/control';
               ;;                 )
               (bluetooth-service)
               ;; (service cups-service-type
               ;;          (cups-configuration
               ;;           (web-interface? #t)
               ;;           ;; hplip depends on a lot of Qt libraries.  Use hplip-minimal instead?
               ;;           ;; (extensions
               ;;           ;;  (list cups-filters escpr hplip))
               ;;           ))
               (service tor-service-type
                        (tor-configuration
                         (config-file (plain-file "tor.conf" %mimimi/tor-config))))
               %mimimi/services))

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)))
