;; With Eshell:
;; *sudo -E guix system reconfigure ~/.guix-packages/ambrevar/system/desktop-bababa.scm
(define-module (ambrevar system desktop-bababa)
  #:use-module (ambrevar system default)
  #:use-module (nonfree packages linux)
  #:use-module (gnu system)
  #:use-module (gnu bootloader)
  #:use-module (gnu bootloader grub)
  #:use-module (gnu packages linux)
  #:use-module (gnu system file-systems))

(operating-system
  (inherit default-operating-system)
  (host-name "bababa")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (timeout 1)
               (target "/dev/sdb")))

  ;; (kernel linux-libre)
  ;; (firmware %base-firmware)
  (firmware (append (list linux-firmware-amdgpu)
                    %base-firmware))

  (file-systems (cons* (file-system
                         (device (file-system-label "guixsd"))
                         (mount-point "/")
                         (type "ext4"))
                       (file-system
                         (device (file-system-label "babadata"))
                         (mount-point "/media/babadata")
                         (type "ext4")
                         (create-mount-point? #t))
                       %base-file-systems)))
