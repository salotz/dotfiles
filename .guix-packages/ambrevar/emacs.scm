(define-module (ambrevar emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages emacs)
  #:use-module (guix download)
  #:use-module (guix git-download))

(define-public ambrevar-emacs
  (package
    (inherit emacs)
    (name "ambrevar-emacs")
    (inputs
     `(("libxaw" ,libxaw)
       ,@(package-inputs emacs)))
    (arguments
     `(#:configure-flags
       (list ;; "--with-x-toolkit=athena"
             ;; "--with-xaw3d"             ; Optional for Athena.
             ;; "--with-x-toolkit=lucid"
             "--with-x-toolkit=no"
             ;; "--with-xwidgets" ; Requires GTK3.
             )
       ,@(package-arguments emacs)))
    (synopsis "Emacs with Athena GUI")))

(define-public clhs
  (package
    (name "clhs")
    (version "0.6.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://beta.quicklisp.org/archive/clhs/2015-04-07/clhs-"
                           version
                           ".tgz"))
       (sha256
        (base32
         "1cn5bfrcawrbc8s1wb07lpr6xv8758l1n5pgkyhamagmi1r0x128"))))
    (build-system emacs-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'replace-loader
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (delete-file "clhs-use-local.el")
             (with-output-to-file "clhs.el"
               (lambda ()
                 (display
                  (string-append
                   "(defun clhs-setup ()
  (setq common-lisp-hyperspec-root
        \"file://"
                   (string-append (assoc-ref outputs "out")
                                  "/HyperSpec-7-0/HyperSpec/")
                   "\"))\n"
                   "(provide 'clhs)"))))))
         (add-after 'install 'install-doc
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (copy-recursively "HyperSpec-7-0"
                               (string-append (assoc-ref outputs "out")
                                              ;; "/share/emacs/site-lisp/guix.d/clhs-"
                                              ;; ,version
                                              "/HyperSpec-7-0")))))))
    (home-page "http://quickdocs.org/clhs/")
    (synopsis "Offline Common Lisp HyperSpec")
    (description
     "Offline Common Lisp HyperSpec.
Setup with:
  (require 'clhs)
  (clhs-setup)")
    (license license:unlicense)))
