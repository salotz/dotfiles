(define-module (ambrevar tex)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages tex)
  #:use-module (guix build-system texlive)
  #:use-module (guix build-system trivial)
  #:use-module (guix svn-download)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 match)
  #:use-module ((srfi srfi-1) #:hide (zip)))

;; TODO: Marvosym fonts build but they can't be used properly.
;; http://www.tug.org/svn/texlive/tags/texlive-2017.1/Master/texmf-dist/source/fonts/marvosym/
;; https://github.com/mojca/marvosym
;; Check out texlive-fonts-ec, we need to patch some PATHS.
(define-public texlive-fonts-marvosym
  (package
    (name "texlive-fonts-marvosym")
    (version (number->string %texlive-revision))
    (source (origin
              (method svn-fetch)
              (uri (svn-reference
                    (url (string-append "svn://www.tug.org/texlive/tags/"
                                        %texlive-tag "/Master/texmf-dist/"
                                        "/tex/latex/marvosym"))
                    (revision %texlive-revision)))
              (file-name (string-append name "-" version "-checkout"))
              (sha256
               (base32
                "0qhrccc340ipmq93jh8dmpai81dk46wi34impd6lrzx72fi17s7g"))))
    (build-system trivial-build-system)
    (arguments
     `(#:modules ((guix build utils)
                  (ice-9 match))
       #:builder
       (begin
         (use-modules (guix build utils)
                      (ice-9 match))
         (let ((root (string-append (assoc-ref %outputs "out")
                                    "/share/texmf-dist/"))
               (pkgs '(("source"        . "tex/latex/marvosym")
                       ("marvosym-truetype"    . "fonts/truetype/public/marvosym")
                       ("marvosym-afm"   . "fonts/afm/public/marvosym")
                       ("marvosym-tfm"   . "fonts/tfm/public/marvosym")
                       ("marvosym-type1" . "fonts/type1/public/marvosym")
                       ;; ("marvosym-enc"   . "fonts/enc/dvips/marvosym")
                       ("marvosym-map"   . "fonts/map/dvips/marvosym"))))
           (for-each (match-lambda
                       ((pkg . dir)
                        (let ((target (string-append root dir)))
                          (mkdir-p target)
                          (copy-recursively (assoc-ref %build-inputs pkg)
                                            target))))
                     pkgs)
           #t))))
    (native-inputs
     `(("marvosym-tfm"
        ,(origin
           (method svn-fetch)
           (uri (svn-reference
                 (url (string-append "svn://www.tug.org/texlive/tags/"
                                     %texlive-tag "/Master/texmf-dist/"
                                     "/fonts/tfm/public/marvosym"))
                 (revision %texlive-revision)))
           (file-name (string-append name "-tfm-" version "-checkout"))
           (sha256
            (base32
             "1912j5p59baij47cr793jsjsp465077g990iif6vm6bgg7ha8b2v"))))
       ("marvosym-afm"
        ,(origin
           (method svn-fetch)
           (uri (svn-reference
                 (url (string-append "svn://www.tug.org/texlive/tags/"
                                     %texlive-tag "/Master/texmf-dist/"
                                     "/fonts/afm/public/marvosym"))
                 (revision %texlive-revision)))
           (file-name (string-append name "-afm-" version "-checkout"))
           (sha256
            (base32
             "09jb393ivgnk4brx8jgll5dpfx2hqb2h94i94lqv30snbnhw93k8"))))
       ("marvosym-type1"
        ,(origin
           (method svn-fetch)
           (uri (svn-reference
                 (url (string-append "svn://www.tug.org/texlive/tags/"
                                     %texlive-tag "/Master/texmf-dist/"
                                     "/fonts/type1/public/marvosym"))
                 (revision %texlive-revision)))
           (file-name (string-append name "-type1-" version "-checkout"))
           (sha256
            (base32
             "19kri8lf2z6j3b74iczppj01j28m3v2qbwq9507nxswfjxxlmb22"))))
       ("marvosym-truetype"
        ,(origin
           (method svn-fetch)
           (uri (svn-reference
                 (url (string-append "svn://www.tug.org/texlive/tags/"
                                     %texlive-tag "/Master/texmf-dist/"
                                     "/fonts/truetype/public/marvosym"))
                 (revision %texlive-revision)))
           (file-name (string-append name "-truetype-" version "-checkout"))
           (sha256
            (base32
             "1x4yrpwwjfvhfvcby9w4dv0kdsgz0ic0c0i5zg1h692grvc0rzar"))))
       ("marvosym-map"
        ,(origin
           (method svn-fetch)
           (uri (svn-reference
                 (url (string-append "svn://www.tug.org/texlive/tags/"
                                     %texlive-tag "/Master/texmf-dist/"
                                     "/fonts/map/dvips/marvosym"))
                 (revision %texlive-revision)))
           (file-name (string-append name "-map-" version "-checkout"))
           (sha256
            (base32
             "1ybwqpwmr79ma9sf0c7apyadhldzsxbwbqgnracaiy810mjzychf"))))))
    (home-page "https://www.ctan.org/pkg/threeparttable")
    (synopsis "Martin Vogel's Symbols (marvosym) font")
    (description
     "Martin Vogel’s Symbol font (marvosym) contains the Euro currency symbol as
;; defined by the European commission, along with symbols for structural
;; engineering; symbols for steel cross-sections; astronomy signs (sun, moon,
;; planets); the 12 signs of the zodiac; scissor symbols; CE sign and others.

;; The package contains both the original TrueType font and the derived Type 1
;; font, together with support files for TeX (LaTeX).")
    ;; TODO: Fix license.
    (license license:gpl3+)))
