;; Gandi SMTP
;; https://docs.gandi.net/en/gandimail/standard_email_settings/index.html

(setq smtpmail-smtp-server  "mail.gandi.net"
      smtpmail-stream-type 'starttls
      smtpmail-smtp-service 587)

;; This is only useful to distinguish between similar entries in .authinfo.
;; (setq smtpmail-smtp-user user-mail-address)

;; REVIEW: If we don't set `user-mail-address', `mail-host-address' or
;; `message-user-fqdn', `message-make-fqdn' will put
;; "i-did-not-set--mail-host-address--so-tickle-me" in the In-Reply-To header.
(setq user-mail-address "mail@ambrevar.xyz")

(provide 'init-smtpmail)
