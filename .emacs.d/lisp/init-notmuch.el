;; Notmuch

(require 'init-message)
(require 'patch-notmuch)
(require 'init-notmuch-sync)

;; To find files matching email:
;;   notmuch search --output=files FOO
;; The following is good enough for multiple-account support if they use the
;; same SMTP server.
(setq notmuch-fcc-dirs
      '(("mail@ambrevar.xyz" . "mail/Sent +sent -inbox -unread")
        ("pierre@atlas.engineer" . "atlas/Sent +sent -inbox -unread")))

(setq notmuch-saved-searches
      `((:name "inbox" :query "tag:inbox and date:1w.." :key ,(kbd "i"))
        (:name "unread" :query "tag:unread" :key ,(kbd "u"))
        (:name "flagged" :query "tag:flagged" :key ,(kbd "f"))
        (:name "sent" :query "tag:sent and date:1w.." :key ,(kbd "t"))
        (:name "drafts" :query "tag:draft" :key ,(kbd "d"))
        (:name "all mail" :query "date:2w.." :key ,(kbd "a"))))

(defun notmuch-change-sender ()
  (interactive)
  (unless (derived-mode-p 'message-mode)
    (error "Must be in message mode"))
  (let ((sender (completing-read "Sender: " (mapcar 'car notmuch-fcc-dirs))))
    (message-replace-header  "From" sender)
    (message-remove-header "Fcc")
    (notmuch-fcc-header-setup)))

(when (require 'patch-helm nil 'noerror)
  (helm-defswitcher
   "notmuch"
   #'notmuch-interesting-buffer
   notmuch-hello))

(when (require 'helm-notmuch nil t)
  (setq helm-notmuch-match-incomplete-words t)
  (dolist (map (list notmuch-search-mode-map
                     notmuch-hello-mode-map
                     notmuch-show-mode-map
                     notmuch-tree-mode-map))
    (define-key map "s" 'helm-notmuch))
  (define-key notmuch-show-mode-map (kbd "M-s f") #'helm-imenu))

(when (require 'org-notmuch nil 'noerror)
  (dolist (map (list notmuch-show-mode-map notmuch-tree-mode-map))
    (define-key map (kbd "C-c C-t") 'org-capture))
  (add-to-list 'org-capture-templates
               `("t" "Mark e-mail in agenda" entry (file+headline ,(car org-agenda-files) "E-mails")
                 "* %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"++7d\" nil (notmuch-show-get-date)))\n%a\n")))


(defun notmuch-show-bounce (&optional address)
  "Bounce the current message."
  (interactive "sBounce To: ")
  (notmuch-show-view-raw-message)
  (message-resend address))

(define-key notmuch-show-mode-map "b" #'notmuch-show-bounce)

;; Improve address completion with Helm.
(setq notmuch-address-use-company nil)
(setq notmuch-address-selection-function
      (lambda (prompt collection initial-input)
        (completing-read prompt (cons initial-input collection) nil t nil 'notmuch-address-history)))

(provide 'init-notmuch)
